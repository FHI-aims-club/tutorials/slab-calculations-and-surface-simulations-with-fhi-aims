from pymatgen.core.surface import SlabGenerator, Lattice, Structure
from pymatgen.io.aims.inputs import AimsGeometryIn
import os

# Create the diamond Si bulk first
lattice = Lattice.cubic(5.43)
Si = Structure(
    lattice,
    ["Si", "Si", "Si", "Si", "Si", "Si", "Si", "Si"],
    [
        [0.00000, 0.00000, 0.50000],
        [0.75000, 0.75000, 0.75000],
        [0.00000, 0.50000, 0.00000],
        [0.75000, 0.25000, 0.25000],
        [0.50000, 0.00000, 0.00000],
        [0.25000, 0.75000, 0.25000],
        [0.50000, 0.50000, 0.50000],
        [0.25000, 0.25000, 0.75000],
    ],
)

# Now we use the pymatgen SlabGenerator to create the Si(100) surface
slabgen = SlabGenerator(
    Si,
    (1, 0, 0),
    min_slab_size=2,
    min_vacuum_size=6,
    in_unit_planes=True,
    center_slab=True,
)
Si_100 = slabgen.get_slabs()[0]

# Pymatgen supports the aims input/output format
# Write geometry.in file in current dir
Si_100_aims = AimsGeometryIn.from_structure(Si_100)
Si_100_aims.write_file(".")

os.rename("geometry.in", "Si_100.in")
