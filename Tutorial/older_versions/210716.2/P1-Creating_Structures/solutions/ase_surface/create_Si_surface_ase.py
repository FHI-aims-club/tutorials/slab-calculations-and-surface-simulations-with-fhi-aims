from ase.build import diamond100

# Create the diamond 100 surface of Si directly using an utility function
sur = diamond100("Si", [1,1,7], a=5.43, vacuum = 20.)

sur.write("Si_100.in", scaled=True)
