# Slab Calculations and Surface Simulations with FHI-aims

This tutorial introduces the main concepts of ab initio surface simulations and slab creation. This tutorial can be used as preparation for the tutorial [Introduction of ab initio thermodynamics and REGC](https://fhi-aims-club.gitlab.io/tutorials/introduction-of-ab-initio-thermodynamics-and-regc).

This tutorial was a joint work by (in alphabetical order) Volker Blum, Sebastian Kokott, and Konstantin Lion.

## Objectives of This Tutorial

* Creating slab structures for FHI-aims
* Slab calculations in FHI-aims


## Prerequisites

* A sufficient understanding about the basics of running FHI-aims is required. Please review our tutorial [Basics of Running FHI-aims tutorial](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/) if you have not yet done so and/or if you are unfamiliar with the code.

* A sufficiently powerful computer. For this tutorial a laptop with at least two (physical cores) should be sufficient.

* The FHI-aims code distribution must be present on your computer.

* A python installation with the [ASE](https://wiki.fysik.dtu.dk/ase/index.html) and/or [pymatgen](https://pymatgen.org) libraries.

The complete set of input and output files of this tutorial are available at <https://gitlab.com/FHI-aims-club/tutorials/slab-calculations-and-surface-simulations-with-fhi-aims>, which can be easily obtained by cloning this repository onto your own computer.

## Useful links

The following links contain useful information and tools regarding this tutorial and FHI-aims in general:

- The present gitlab repository, which contains all the documents and simulation data for this tutorial: <https://gitlab.com/FHI-aims-club/tutorials/introduction-of-ab-initio-thermodynamics-and-regc> 
- The CLIMS gitlab repository, which contains useful utilities that can be used alongside with FHI-aims to facilitate input files preparation and/or output data analysis, in particular [CLIMS](https://gitlab.com/FHI-aims-club/utilities/clims): <https://gitlab.com/FHI-aims-club/utilities>
- FHI-aims browser-based Graphical Interface for Materials Simulation (GIMS): <https://gims.ms1p.org>

