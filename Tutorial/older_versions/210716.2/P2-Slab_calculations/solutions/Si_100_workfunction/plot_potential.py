import numpy as np
import matplotlib.pyplot as plt

bohr_to_angstrom = 0.529177

# Read data
data = np.genfromtxt("plavg_Z.dat", unpack=True)

data[0] = data[0] * bohr_to_angstrom
# Plot data
fig = plt.figure()
ax = fig.add_subplot(111)
# ax2 = fig.add_subplot(211)
plt.xlabel(r"$z$ [$\AA$]")
plt.ylabel(r"Potential [eV]")
plt.ylim(-15., 4)
plt.xlim(min(data[0]), max(data[0]))
# ax.xaxis.set_minor_locator(AutoMinorLocator(2))
# ax.yaxis.set_minor_locator(AutoMinorLocator(1))

# plot_atoms(struc, ax, radii=0.3, rotation=('90x,90y,0z'), show_unit_cell=0)
ax.plot(data[0], data[1], c="black")
plt.axhline(y=-5.51395825, ls="--", c="black")

ax.spines['top'].set_visible(False)

# plt.legend()
plt.savefig("planer_average.png", dpi=300)
